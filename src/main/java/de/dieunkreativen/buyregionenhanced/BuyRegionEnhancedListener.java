/*
* BuyRegionEnhanced allows you to create signs which can be used to buy WorldGuard regions
* Copyright (C) Paaattiii <http://www.dieunkreativen.de>
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package de.dieunkreativen.buyregionenhanced;

import net.milkbowl.vault.economy.EconomyResponse;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.sk89q.worldedit.Vector;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.managers.storage.StorageException;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class BuyRegionEnhancedListener implements Listener  {
	public BuyRegionEnhanced plugin;

		
	public BuyRegionEnhancedListener(BuyRegionEnhanced instance) {
		plugin = instance;
	}
	

	
	@EventHandler
	public void onSignPlace(SignChangeEvent event) {
		Location loc = event.getBlock().getLocation();
		Player p = event.getPlayer();
		
		if (p.isOp() || p.hasPermission("buyregion.create")) {
			if (event.getLine(0).equalsIgnoreCase("gs")) {
				if (!(getSignRegion(loc) == null)) {
					event.setLine(0, ChatColor.GREEN + "[Grundst�ck]");
					if (event.getLine(1).length() > 0) {
						event.setLine(1, "Preis: " + event.getLine(1));
					}
					else {
						event.setLine(1, "Preis: 500");
					}
					event.setLine(2, getSignRegion(loc));
					clearOwnerandMember(getSignRegion(loc), p);
					p.sendMessage(ChatColor.GREEN + "Das Grundst�ck steht nun zum Verkauf!");
				}
				else {
					event.getBlock().breakNaturally();
                    event.setCancelled(true);
                    p.sendMessage(ChatColor.RED + "Hier ist keine g�ltige Region!");
				}
			}
		}
		
	}
	
	@EventHandler
	public void onPunchSign(PlayerInteractEvent event) {
		 if (event.getAction().name() == "RIGHT_CLICK_BLOCK") {
			 Material blockType = event.getClickedBlock().getType();
		     if ((blockType == Material.SIGN_POST) || (blockType == Material.WALL_SIGN)) {
		          Sign sign = (Sign)event.getClickedBlock().getState();
		          String topLine = sign.getLine(0);
		          if(topLine.equalsIgnoreCase(ChatColor.GREEN + "[Grundst�ck]")) {
		        	  double regionPrice = Double.parseDouble(sign.getLine(1).split(" ")[1]);
		        	  String regionName = sign.getLine(2);
		        	  Player p = event.getPlayer();
		        	  if(setOwner(regionPrice, regionName,p)) {
		        		  updateSign(sign,p);
		        		  setRegionCount(p);
		        	  }
		          }
		     }
		 }
	}
	
	public boolean setOwner(double regionPrice, String regionName, Player p) {
		if (BuyRegionEnhanced.wp != null) {
  		  RegionManager rm = BuyRegionEnhanced.wp.getRegionManager(p.getWorld());
  		  ProtectedRegion region = rm.getRegion(regionName);
  		  DefaultDomain dd = new DefaultDomain();
            dd.addPlayer(BuyRegionEnhanced.wp.wrapPlayer(p));
  		  if (region != null) {
  			  if (plugin.getConfig().getInt(p.getUniqueId().toString() + ".purchasedRegions") < 1) {
  				  if (BuyRegionEnhanced.econ.getBalance(p) >= regionPrice) {
  					  EconomyResponse response = BuyRegionEnhanced.econ.withdrawPlayer(p, regionPrice);
  					  if (response.transactionSuccess()) {
  						  region.setOwners(dd);
  						  try {
  							  rm.save();
  							  p.sendMessage(ChatColor.GREEN + "Grundst�ck " + ChatColor.RED + regionName + ChatColor.GREEN + " geh�rt nun dir!");
  							  p.sendMessage(ChatColor.GREEN + "Vergess nicht dein Zuhause zu setzen mit " + ChatColor.RED + "/sethome <name>");
  							  return true;
  						  } catch (StorageException e) {
  							  // TODO Auto-generated catch block
  							  e.printStackTrace();
  						  }
  					  }
  				  }
  				  else {
  					  p.sendMessage(ChatColor.RED + "Du hast nicht gen�gend Geld!");
  				  }
  			  }
  			  else {
  				  p.sendMessage(ChatColor.RED + "Frage einen Supporter, Moderator oder Admin f�r ein weiteres Grundst�ck");
  			  }
  		  }
		}
		return false;
	}
	
	public void clearOwnerandMember(String regionName, Player p) {
		RegionManager rm = BuyRegionEnhanced.wp.getRegionManager(p.getWorld());
		ProtectedRegion region = rm.getRegion(regionName);
		region.getOwners().removeAll();
		region.getMembers().removeAll();
	}
	
	public void setRegionCount(Player p) {
		FileConfiguration config = BuyRegionEnhanced.plugin.getConfig();
		config.set(p.getUniqueId().toString() + ".name", p.getName());
		config.set(p.getUniqueId().toString() + ".purchasedRegions", 1);
		plugin.saveConfig();
	}
	
	
	
	public void updateSign(Sign sign, Player p) {
		sign.setLine(0, ChatColor.WHITE + "##############");
		sign.setLine(1, ChatColor.RED + "Verkauft an:");
		sign.setLine(2, p.getName());
		sign.setLine(3, ChatColor.WHITE + "##############");
		sign.update();
	}
	
	private String getSignRegion(Location loc) {
      // Make sure that a WorldGuard instance was found
		 if (BuyRegionEnhanced.wp != null) {
			 // Get the players position (as a WorldEdit Vector)
			 Vector newVector = new Vector(loc.getX(), loc.getY(), loc.getZ());     
       
			 // Get WorldGuards Region Manager
			 RegionManager rm = BuyRegionEnhanced.wp.getRegionManager(loc.getWorld());
       
			 // Make sure a RegionManager was found
			 if (rm != null)    {
				 ApplicableRegionSet set = rm.getApplicableRegions(newVector);
				 for (ProtectedRegion each : set)
					 return each.getId();
             	 
			 }
		 }
		 return null;
	 }
	 
}

