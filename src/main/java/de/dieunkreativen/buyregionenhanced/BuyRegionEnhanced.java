/*
* BuyRegionEnhanced allows you to create signs which can be used to buy WorldGuard regions
* Copyright (C) Paaattiii <http://www.dieunkreativen.de>
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package de.dieunkreativen.buyregionenhanced;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.util.profile.cache.ProfileCache;

public class BuyRegionEnhanced extends JavaPlugin {
	Logger log;
	public static BuyRegionEnhanced plugin;
	public static Economy econ = null;
	public static WorldGuardPlugin wp = null;
	ProfileCache pc;
	PluginDescriptionFile pdfFile = this.getDescription();
	private final BuyRegionEnhancedListener listener = new BuyRegionEnhancedListener(this);
	
    @Override
    public void onLoad() {
            log = getLogger();
    }
    
	@Override
	public void onEnable() {
		plugin = this;
		setupEconomy();
		setupWorldGuard();
		PluginManager pm = this.getServer().getPluginManager();
		pm.registerEvents(listener, this);
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled!");

	}
	
    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
    
	 public boolean setupWorldGuard() {
		 	Plugin plugin = BuyRegionEnhanced.getSelf().getServer().getPluginManager().getPlugin("WorldGuard");
		     
		    if ((plugin == null) || (!(plugin instanceof WorldGuardPlugin))) {
		    	return false;
		    }
		     
		    wp = (WorldGuardPlugin)plugin;
		    return true;
	 }
    
	@Override
	public void onDisable() {
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is disabled!");
	}
	
	public static BuyRegionEnhanced getSelf() {
		return plugin;
	}
	
	@Override
	public boolean onCommand(final CommandSender sender, Command cmd, String label, String[] args) {	
		if (cmd.getName().equalsIgnoreCase("gs")) {
			if(args.length > 0) {
				if(args[0].equalsIgnoreCase("reset") && sender.hasPermission("buyregion.use")) {
					if(args.length == 2) {
						String p = (args[1]);
						UUID uuid = getUUID(p);
				        if (uuid != null) {
				        	plugin.getConfig().set(uuid.toString(), null);
				            plugin.saveConfig();
				            sender.sendMessage(ChatColor.GREEN + p + " kann sich nun ein weiteres Grundst�ck kaufen.");
				            if(isPlayerOnline(p)) {
				            	Bukkit.getPlayer(p).sendMessage(ChatColor.GREEN + "Du bist f�r ein weiteres Grundst�ck freigeschalten!");
				            }
				        }
				        else {
				        	sender.sendMessage(ChatColor.RED + p + " war noch nie auf diesen Server!");
				        }
					}
					else  {
						sender.sendMessage(ChatColor.RED + "/gs reset <player>");
					}
					return true;
				}
				
				if(args[0].equalsIgnoreCase("set") && sender.hasPermission("buyregion.use")) {
					if(args.length == 2) {
						String p = (args[1]);
						UUID uuid = getUUID(p);
				        if (uuid != null) {
				        	plugin.getConfig().set(uuid.toString() + ".name", p);
							plugin.getConfig().set(uuid.toString() + ".purchasedRegions", 1);
							plugin.saveConfig();
							sender.sendMessage(ChatColor.GREEN + p + " f�r weitere Grundst�cke gesperrt.");
				        }
					}
					else {
						sender.sendMessage(ChatColor.RED + "/gs set <player>");
					}
					return true;
				}
				
				if(args[0].equalsIgnoreCase("kaufen")) {
					if(sender instanceof Player) {
						Player player = (Player) sender;
						if(getRandomFreeRegion(player).size() > 0) {
							int randomint = (int) Math.abs(Math.random()*getRandomFreeRegion(player).size());
							String randomRegion = getRandomFreeRegion(player).get(randomint);
							player.teleport(getCenterOfRegion(randomRegion, player.getWorld()));
							player.sendMessage("==============================");
							player.sendMessage(ChatColor.GREEN + "Zurzeit sind " + ChatColor.RED + getRandomFreeRegion(player).size() + 
									" Grundst�cke " + ChatColor.GREEN + "frei!");
							player.sendMessage(ChatColor.GREEN + "Du befindest dich auf " + ChatColor.RED + randomRegion + ChatColor.GREEN + ".");
							player.sendMessage(ChatColor.GREEN + "Ist es sch�n hier? Dann kauf es mit einem Rechtsklick auf das Schild! :D");
							player.sendMessage(ChatColor.GREEN + "Oder lass dich zum n�chsten Teleportieren mit " +
									ChatColor.RED + "/gs kaufen" + ChatColor.GREEN + ".");
							player.sendMessage("==============================");
						}
						else {
							player.sendMessage(ChatColor.RED + "In der Welt " + "'" + player.getWorld().getName() + "'" + " kann man keine Grundst�cke kaufen!");
						}
					}
				}
				
				if(args[0].equalsIgnoreCase("info") && sender.hasPermission("buyregion.use")) {
					if(args.length == 2) {
						if(sender instanceof Player) {
							String player = (args[1]);
							Player p = (Player) sender;
							ArrayList<String> regions = getRegions(p, player);
							if(regions != null) {
								p.sendMessage(ChatColor.GREEN + player + ChatColor.GOLD + " ist in Welt " + "'" + p.getWorld().getName() + "'" + " auf " + ChatColor.GREEN + regions.size() + " Regionen " + ChatColor.GOLD + "als Owner eingetragen:");
								p.sendMessage(regions.toString());
								if (isPlayerOnline(player)) {
									if (plugin.getConfig().getInt(Bukkit.getPlayer(player).getUniqueId().toString() + ".purchasedRegions") < 1) {
										p.sendMessage(ChatColor.GOLD + "Kann sich Grundst�ck kaufen: " + ChatColor.GREEN + "Ja");
									}
									else {
										p.sendMessage(ChatColor.GOLD + "Kann sich Grundst�ck kaufen: " + ChatColor.RED + "Nein");
									}
								}
							}
							else {
								p.sendMessage(ChatColor.RED + player + " war noch nie auf diesen Server!");
							}
						}
					}
					else {
						sender.sendMessage(ChatColor.RED + "/gs info <player>");
					}
				}
				
				if(args[0].equalsIgnoreCase("tp") && sender.hasPermission("buyregion.use")) {
					if(args.length == 2) {
						if(sender instanceof Player) {
							String region = args[1];
							Player p = (Player) sender;
							
							
							Location loc = getCenterOfRegion(region, p.getWorld());
							if(loc != null) {
								p.teleport(loc);
								p.sendMessage(ChatColor.GREEN + "Teleportiere zu " + region + "...");
							}
							else {
								p.sendMessage(ChatColor.RED + "Diese Region existiert in Welt " + "'" + p.getWorld().getName() + "'" + " nicht!");
							}
							
						}
					}
					else {
						sender.sendMessage(ChatColor.RED + "/gs tp <ID>");
					}
				}
			}
			else {
				if(sender instanceof Player && sender.hasPermission("buyregion.use")) {
					Player player = (Player) sender;
					player.getInventory().addItem(new ItemStack(Material.SIGN, 1));
				}
				else {
					sender.sendMessage(ChatColor.RED + "/gs reset | set");	
				}
				
			} 
		}
		return false;
	}
	
	public Location getCenterOfRegion(String region, World world) {
		ProtectedRegion pr = wp.getRegionManager(world).getRegion(region);
		
		if (pr == null) {
			return null;
		}
           
		int minX = pr.getMinimumPoint().getBlockX();
    	int minZ = pr.getMinimumPoint().getBlockZ();
		
		int maxX = pr.getMaximumPoint().getBlockX();
		int maxZ = pr.getMaximumPoint().getBlockZ();
		
		double centerX = ((maxX - minX) / 2) + minX;
		double centerZ = ((maxZ - minZ) / 2) + minZ;
		
		return new Location(world, centerX, 42, centerZ);
	}
	
	public ArrayList<String> getRegions(Player p, String player) {
		ArrayList<String> regionlist = new ArrayList<String>();
		Map<String, ProtectedRegion> set = wp.getRegionManager(p.getWorld()).getRegions();
		UUID uuid = getUUID(player);
        if (uuid == null) {
        	return null;
        }
        String puuid = getUUID(player).toString();
		for (Map.Entry<String, ProtectedRegion> e : set.entrySet()) {
			String name = (String)e.getKey();
			ProtectedRegion pr = (ProtectedRegion)e.getValue();
			String owners = pr.getOwners().toUserFriendlyString(pc);
			if(owners.contains(puuid)) {
				regionlist.add(name);
			}
				
		}
		return regionlist;
	}
	
	public ArrayList<String> getRandomFreeRegion(Player p) {
		Map<String, ProtectedRegion> set = wp.getRegionManager(p.getWorld()).getRegions();
		ArrayList<String> regionlist = new ArrayList<String>();
		
		for (Map.Entry<String, ProtectedRegion> e : set.entrySet()) {
			String name = (String)e.getKey();
			ProtectedRegion pr = (ProtectedRegion)e.getValue();
			if(pr.getOwners().size() == 0) {
				if(name.substring(0,2).equalsIgnoreCase("gs") && isInteger(name.substring(2))) {
					regionlist.add(name);
				}
			}
		}
		return regionlist;
	}
	
	@SuppressWarnings("deprecation")
	public UUID getUUID(String p) {
    	if (isPlayerOnline(p)) {
    		return Bukkit.getPlayer(p).getUniqueId();
    	}
    	else {
    		OfflinePlayer op = Bukkit.getOfflinePlayer(p);
    		if (op.hasPlayedBefore()) {
    			return op.getUniqueId();
    		}
    	}
    	return null;
	}
	
	@SuppressWarnings("deprecation")
	public boolean isPlayerOnline(String name){
		for (Player onlinePlayer : Bukkit.getOnlinePlayers()){
			if (onlinePlayer.getName().equalsIgnoreCase(name))
				return true;
		}
		return false;
	}
	
    public boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}

